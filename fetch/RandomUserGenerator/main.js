var url = "https://randomuser.me/api/";

var btn = document.querySelector("#btn");
var fullname = document.querySelector("#fullname");
var username = document.querySelector("#username");
var email = document.querySelector("#email");
var city = document.querySelector("#city");
var avatar = document.querySelector("#avatar");

btn.addEventListener("click", function(){
 fetch(url)
    .then(handleErrors)
    .then(parsJSON)
    .then(updateProfile)
    .catch(displayErrors)
});
function handleErrors(res){
    if(!res.ok){
        throw Error(res.status);
    }
    return res;
}

function parsJSON(res){
    return res.json().then(function(parsedDatadata){
        return parsedDatadata.results[0];
    });
}
function updateProfile(data){
        fullname.innerText = data.name.first +" " + data.name.last;
        username.innerText = data.login.username;
        email.innerText = data.email;
        city.innerText = data.location.city;
        avatar.src =  data.picture.medium;
}
function displayErrors(err){
        console.log(err);
}


