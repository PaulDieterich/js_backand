$(document).ready(function(){
    var url = 'https://ron-swanson-quotes.herokuapp.com/v2/quotes';


    $("#xhr").click( function(){
        var XHR = new XMLHttpRequest();

        XHR.onreadystatechange = function(){
            if( XHR.status == 200 && XHR.readyState == 4){
                var res = JSON.parse(XHR.responseText)[0];
               
                console.log("xhr --- " +res);
                $("#quote").text(res);
            }
        
        }
        XHR.open("GET", url);
        XHR.send();
    });
    $("#fetch").click( function(){
        fetch(url)
        .then(function(res){
            return res.json().then(function(data){
                $("#quote").text(data[0]);
                console.log("fetch --- " +data[0]);
            })
        })
    });
    $("#jquery").click( function(){
        $.getJSON(url)
        .done(function(res){
            $("#quote").text(res[0]);
            console.log("jquery --- " +res[0]);
        })
    });

    $("#axios").click( function(){
        axios.get(url)
        .then( function(res){
            $("#quote").text(res.data[0]);
            console.log("axios --- " +res.data[0]);
        });
    });

});